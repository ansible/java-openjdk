# java-openjdk

Ansible role that installs Java and OpenJDK/JRE from the Ubuntu repos
or from the OpenJDK PPA (by setting a variable, see `defaults/main.yml`).
